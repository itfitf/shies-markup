$(document).ready(function () {
    svg4everybody({});

    // раскрытие поля поиска start
    let searchInputVal = $('.header-search__input').val();
    if (searchInputVal !== '') {
        $('.header-search__input').addClass('active');
    }
    $(".header-search__btn").on("click", function(){
        let searchInputVal = $('.header-search__input').val();
        $('.header-search__input').select();
        if (searchInputVal !== '') {
            $(".header-search__btn").get(0).type = 'submit';
        }
        if ($(window).width() > 992) {
            $('.header-search__input').animate({ width: "30rem"} , 1000);
            $('.header-search__input').addClass('active');
        }
    });
    // раскрытие поля поиска end

    // Вызов бокового меню
    if ($(window).width() < 992) {
        $('.side-bar-button,#block-layer,.info__link').click(function(j) {
            $('.header-main,#block-layer,.side-bar-button').toggleClass('open');
            $('body').toggleClass('ovh');
        });
    }
    // end

    // выпадающее меню
    $( ".nav__item:has(.sub-menu)" ).addClass( "has-sub-menu" ); // если есть вложенное меню то добавляем класс

    if ($(window).width() < 992) {
        $('.nav__item').removeClass('closed')
        $('.nav__item').bind({
            click: function () {
                $(this).find('.sub-menu').stop(true, true).delay(60).fadeIn(60);
                $(this).toggleClass("active").animate(100);
                $(this).toggleClass("closed").animate(100);
            }, function() {
                $(this).find('.sub-menu').stop(true, true).delay(60).fadeOut(60);
                $(this).toggleClass("active").animate(100);
                $(this).toggleClass("closed").animate(100);
            }
        });// do something
    } else {
        $('.nav__item').hover(function() {
            $(this).find('.sub-menu').stop(true, true).delay(60).fadeIn(60);
            $(this).toggleClass("active").animate(100);
            $(this).toggleClass("closed").animate(100);
        }, function() {
            $(this).find('.sub-menu').stop(true, true).delay(60).fadeOut(60);
            $(this).toggleClass("active").animate(100);
            $(this).toggleClass("closed").animate(100);
        });// do something else, etc.
    }
    // end

    // Вызов плагина для попап-окон видео и трансляций /start
    $('#video-about, #video-process, #video-camera1, #video-camera2, .grid-fluid__item').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: true,
        fixedContentPos: true
    });
    // end
    
    // Добавляет класс каточке ".article" при условии что в ней есть img для изменения цвета текста
    $('.article.grid-card').each(function() {
        if ($(this).find('img.article__bg-img').length) {
            $(this).addClass('article--bg-img');
        }
    });
    // end
    
    // слайдер
    $('#about-slider,#opinion-slider').slick({
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
        infinite: true,
        loop: true,
        slidesToShow: 3,
        slidesToScroll: 1
    });
    // end

    // -//////////////////////////////
    $('#content-slider').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
        mobileFirst: true,
        pauseOnHover: true,
        slidesToShow: 1
    });
    // -//////////////////////////////

    // Плагин для блока вопросов
    $("#accordeon .accordeon-item .accordeon-item__body").hide();
    $("#accordeon > .accordeon-item > .accordeon-item__header").on("click", function(e){
        if($(this).parent().has(".accordeon-item__body")) {
            e.preventDefault();
        }
        var speed =	300;
        if(!$(this).hasClass("active")) {
            // Supprime les classes actives et cache le sous menu
            $("#accordeon .accordeon-item .accordeon-item__body").slideUp(speed);
            $("#accordeon .accordeon-item .accordeon-item__header").removeClass("active");
                    
            // Ajoute les classes actives et ouvre le sous menu
            $(this).next(".accordeon-item__body").slideDown(speed);
            $(this).addClass("active");
        }
                
        else if($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).next(".accordeon-item__body").slideUp(speed);
        }
    });    
    // end

    // start
    $('.dropdown-article__toggle').click(function() {
        $('.dropdown-article__collapsed').slideToggle();
        if ($('.dropdown-article__toggle').text() == "Подробнее") {
            $(this).text("Свернуть")
        } else {
            $(this).text("Подробнее")
        }
    });
    // end

    // Слайдер для видео
    $('#videos-fluid,.block-counter__wrap,#eco-security,#main-parameters').slick({
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
        mobileFirst: true,
        pauseOnHover: true,
        slidesToShow: 1,
        responsive: [
        {
            breakpoint: 767, //at 600px wide, only 2 slides will show
            settings: 'unslick'
        },
        {
            breakpoint: 766, //at 480px wide, only one slide will show
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });
    
    $(window).on('resize orientationchange', function() {
        $('#videos-fluid,.grid-items,.block-counter__wrap').slick('resize');
    });
    // end

    $('#grid-slider').slick({
        dots: false,
        slidesPerRow: 3,
        rows: 2,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 766, //at 480px wide, only one slide will show
                settings: {
                    rows: 1,
                    slidesPerRow: 1,
                }
            }
        ]
    });

    // плавный скролл для якорных ссылок
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
            }
        }
        });
    });
    // плавный скролл для якорных ссылок end

    //галерея  попап start
    $('.gallery-item__preview').magnificPopup({
        type: 'inline',
        midClick: true,
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        midClick: true
    });
    //галерея  попап end

    // swiper-slider в попап
    $('.gallery-top').each(function(index, element){
        $(this).addClass('slider'+index);
    });
    $('.gallery-thumbs').each(function(index, element){
        $(this).addClass('slider-thumbs'+index);
    });

    $('.gallery-item__preview').click(function() {
        $('.gallery-top').each(function(index, element){
            var galleryThumbs = new Swiper('.slider-thumbs'+index, {
                slidesPerView: 7,
                freeMode: true,
                loopedSlides: 5, //looped slides should be the same
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            });
            var galleryTop = new Swiper('.slider'+index, {
                spaceBetween: 10,
                loop:true,
                loopedSlides: 5, //looped slides should be the same
                navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
                },
                thumbs: {
                swiper: galleryThumbs,
                },
            });

        });
    });
    // swiper-slider в попап end

});

// youtube video iframe
function findVideos() {
    let videos = document.querySelectorAll('.video');

    for (let i = 0; i < videos.length; i++) {
        setupVideo(videos[i]);
    }
}

function setupVideo(video) {
    let link = video.querySelector('.video__link');
    let media = video.querySelector('.video__media');
    let button = video.querySelector('.video__button');
    let id = parseMediaURL(media);

    video.addEventListener('click', () => {
        let iframe = createIframe(id);

        link.remove();
        button.remove();
        video.appendChild(iframe);
    });

    link.removeAttribute('href');
    video.classList.add('video--enabled');
}

function parseMediaURL(media) {
    let regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
    let url = media.src;
    let match = url.match(regexp);

    return match[1];
}

function createIframe(id) {
    let iframe = document.createElement('iframe');

    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('allow', 'autoplay');
    iframe.setAttribute('src', generateURL(id));
    iframe.classList.add('video__media');

    return iframe;
}

function generateURL(id) {
    let query = '?rel=0&showinfo=0&autoplay=1';

    return 'https://www.youtube.com/embed/' + id + query;
}

findVideos();
// end

// Яндекс карты
ymaps.ready(init);

var placemarks = [
        {
            latitude: 62.9604,
            longitude: 40.2757,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                // '		<span class="map-balloon__title-light">Архангельская область,</span>',
                // '		<span class="map-balloon__title-light">Обозёрский район,</span>',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                // '	<div class="map-balloon__slider balloon-slider" id="balloon-slider">',
                // '		<div class="balloon-slider__slide">',
                // '			<img src="/wp-content/themes/shies/static/images/content/landfills-map/mini-slide-1.jpg" alt="slide">',
                // '		</div>',
                // '	</div>',
                // '<a href="#">Подробнее<svg class="svg-sprite-icon icon-link-arrow link-arrow"><use xlink:href="/wp-content/themes/shies/static/images/svg/symbol/sprite.svg#link-arrow"></use></svg></a></div></a>',
                '</div>'
            ]
        }, {
            latitude: 62.9596,
            longitude: 40.2716,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Мусор в лесу</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5298,
            longitude: 40.5781,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Куча мусора</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.2288,
            longitude: 41.7193,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.6793,
            longitude: 40.7185,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Мусор в лесу</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.8649,
            longitude: 48.9433,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5559,
            longitude: 40.5762,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Строительный мусор</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5687,
            longitude: 40.5539,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Строительный мусор</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.6505,
            longitude: 40.5566,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Старая свалка</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.6014,
            longitude: 40.5343,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.1465,
            longitude: 50.5203,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка у дороги</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5258,
            longitude: 40.6109,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.7323,
            longitude: 40.0807,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Строительные отходы</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.4586,
            longitude: 40.5844,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Старая свалка</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.6176,
            longitude: 40.6220,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Талаги 99</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.2934,
            longitude: 47.2566,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка на кладбище</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 63.5243,
            longitude: 45.6886,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка мусора</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 63.5090,
            longitude: 45.6901,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 63.5088,
            longitude: 45.6905,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5975,
            longitude: 40.4379,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Бывшая ферма</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5404,
            longitude: 40.5137,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Незаконная свалка</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.5561,
            longitude: 45.9744,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Cвалка за кладбищем</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.0454,
            longitude: 44.0653,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.0472,
            longitude: 44.0669,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.4861,
            longitude: 40.8190,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5461,
            longitude: 40.6085,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5406,
            longitude: 40.5132,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Куча мусора</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.7371,
            longitude: 42.4744,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.6483,
            longitude: 40.2611,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка по лесной дороге</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.6124,
            longitude: 38.2254,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.6410,
            longitude: 40.5247,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка строительного мусора</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5280,
            longitude: 40.5995,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка строительного мусора</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5280,
            longitude: 40.5995,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.5496,
            longitude: 40.6092,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.9067,
            longitude: 48.9247,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 61.8815,
            longitude: 48.9082,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9635,
            longitude: 40.2721,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка 20 куб.м.</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9333,
            longitude: 40.1274,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка 5 куб.м.</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9347,
            longitude: 40.1219,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка 10 куб.м.</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9259,
            longitude: 40.1430,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка 3 куб.м.</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9229,
            longitude: 40.1393,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка 15 куб.м.</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9388,
            longitude: 40.1115,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка 30 куб.м.</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.9635,
            longitude: 40.2694,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 62.1360,
            longitude: 42.7818,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }, {
            latitude: 64.3931,
            longitude: 40.8291,
            hintContent: [
                '<div class="map-balloon">',
                '	<div class="map-balloon__title">',
                '		<strong class="map-balloon__title-bold">Свалка ТБО</strong>',
                '	</div>',
                '</div>'
            ]
        }
    ],
    geoObjects = [];

function init () {
    var map;
    var cluster;
    
    $('#map-trigger,#map-close').bind({
        click: function () {
            if (!map) {
                $('#landfills_map').animate({
                    height: '58rem'
                }, {
                    duration: 500  // 0.5 seconds
                });
                $('#map-descr').slideDown( 'slow', function() {
                    // Animation complete.
                });
                $('body').addClass('map-open');
                $('.landfills-map-overlay').addClass('map-open');
                $('.landfills-map-wrap').addClass('map-open');

                setTimeout(function () {
                map = new ymaps.Map("landfills_map", {
                    center: [64.30164914, 40.20111414],
                    zoom: 7,
                    controls: []
                });
                
                for(let i = 0; i < placemarks.length; i++) {
                    geoObjects[i] = new ymaps.Placemark([placemarks[i].latitude, placemarks[i].longitude], 
                        {
                            hintContent: placemarks[i].hintContent.join('')
                        },{
                            iconLayout: 'default#image',
                            iconImageHref: '/wp-content/themes/shies/static/images/content/landfills-map/map_arrow.svg',
                            iconImageSize: [30, 30],
                            iconImageOffset: [-10, -30],
                        }
                    );
                    
                }
            
                let landfillsClusterer = new ymaps.Clusterer({
                    clusterIcons: [
                        {
                            href: '/wp-content/themes/shies/static/images/content/landfills-map/map_arrow.svg',
                            size: [50, 50],
                            offset: [-20, -50]
                        }
                    ],
                    clusterIconContentLayout: null
                });
            
                map.geoObjects.add(landfillsClusterer);
                landfillsClusterer.add(geoObjects);

                cluster = landfillsClusterer;

                map.events.add('click', function (e) {
                    if($('.landfills-map-wrap').hasClass('map-clickable')) {
                        $('#landfills-popup-form').toggleClass('form-active');

                        if($('#landfills-popup-form').hasClass('form-active')) {
                            var coords = e.get('coords');
                            $('#landfills-popup-form #map-lat').val(coords[0].toPrecision(6));
                            $('#landfills-popup-form #map-long').val(coords[1].toPrecision(6));
                        }
                    }
                });

                }, 510);
            }
            else {
                map.destroy();// Деструктор карты
                map = null;
                $('#landfills_map').animate({
                    height: '0'
                }, {
                    duration: 500  // 2 seconds
                });
                $('body').removeClass('map-open');
                $('.landfills-map-overlay').removeClass('map-open');
                $('.landfills-map-wrap').removeClass('map-open');
                $('#landfills-popup-form').removeClass('form-active');
                $('#map-descr').slideUp( 200, function() {
                    $('.landfills-map-wrap').removeClass('map-clickable');
                });
            }
        }
    });
    $('.landfills-map-overlay').bind({
        click: function () {
            if (map) {
                $('.landfills-map-overlay').removeClass('map-open');
                map.destroy();
                map = null;
                $('.landfills-map-wrap').removeClass('map-open');
                $('body').removeClass('map-open');
                $('#landfills_map').animate({
                    height: '0'
                }, {
                    duration: 500  // 2 seconds
                });
                $('#map-descr').slideUp( 200, function() {
                    // Animation complete.
                });
            }
        }
    });

    // Popup-форма в картах start
    $('.landfills-map-btn').bind({
        click: function () {
            map.geoObjects.removeAll();
            $('.landfills-map-wrap').toggleClass('map-clickable');
        }
    });
    $('#landfills-form-close').bind({
        click: function () {
            $('#landfills-popup-form').removeClass('form-active');
        }
    });
    // Popup-форма в картах end

    $('#landfills-popup-form form').bind({
        submit: function () {
            $.ajax({
                method: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'landfills_form',
                    description: $('#map-field-1').val(),
                    lat: $('#map-lat').val(),
                    long: $('#map-long').val(),
                },
                success: function(response) {
                }
            });

            $('#landfills-popup-form').removeClass('form-active');
            $('.landfills-map-wrap').removeClass('map-clickable');
            $('#landfills-popup-form form')[0].reset();
            map.geoObjects.add(cluster);
            return false;
        }
    });

    $('.map-descr__close').bind({
        click: function () {
            $('#map-descr').slideUp( 200, function() {
                // Animation complete.
            });
        }
    });
}

// плавный скролл для якорных ссылок после загрузки страницы
if(window.location.hash) {
    var hash = window.location.hash;

    $('html, body').animate({
        scrollTop: $(hash).offset().top
    }, 1000, 'swing');
}
// end
